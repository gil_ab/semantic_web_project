from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from SPARQLWrapper import SPARQLWrapper, JSON
from django.utils.safestring import mark_safe
import json, sets, re
from random import randint
import csv, time, hashlib
from django.core.cache import cache
from django.views.decorators.cache import cache_page
from memcached_stats import MemcachedStats
from django.conf import settings
settings.configure()

sparql_endpoint = "http://semtech.mty.itesm.mx:3030/Fototeca/sparql"
# Save values from form
uri = "http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/1005/120621_104829F"
prefix = "edm: <http://purl.org/dc/elements/1.1/>"
varproperty = "edm:identifier"
query_type = 'query2'
iter_number = 20
# Create arrays for saving qery data
concepts = []
qresources1 = []
qresources2 = []
qrelations = []
qpicture_url = []
qpicture_url2 = []
qresources_total = []
qresources_default = []
# Global variables
last_resource = 0
resource1_filter = ""
resource2_filter = ""
results_set = {}
file = csv.writer(open("test_queries.csv", "wb+"))
statistics_file = csv.writer(open("statistics_file.csv", "wb+"))
duplicate_list = []
iteration_number = 0
query_number = 0
key0 = uri
key1 = hashlib.md5()
lista_temporal = []
lista_temporal2 = []


#### Queries' definitions
########################## def query0 ##########################

def query0(prefix, uri, varproperty, sparqlendpoint):
    sparql = SPARQLWrapper(sparqlendpoint)
    sparql.setQuery("""
                        PREFIX """ + prefix + """
                        SELECT ?picture_url (count(?object) as ?relations) ?subject2
                        WHERE { <""" + uri + """>
                            ?property1 ?object  .
                            OPTIONAL { ?subject2 """ + varproperty + """ ?picture_url }.
                            ?subject2 ?property2 ?object .
                            FILTER(?property1 = ?property2)
                            ?subject2 a <http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/Picture> .
                            FILTER(?property1 != <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>)
                            FILTER(?subject2 != <""" + uri + """>)
                        }
                        group by ?subject2 ?picture_url
                        ORDER BY DESC(?relations)
                        LIMIT 10
                    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results

########################## def query1 ##########################
def query1(prefix, subject1, resource2_filter, varproperty, sparqlendpoint):
    sparql = SPARQLWrapper(sparqlendpoint)
    sparql.setQuery("""
                        PREFIX """ + prefix + """
                        SELECT ?resource2 (count(?object) as ?relations) (<"""+ subject1 + """> AS ?resource1) ?picture_url ?picture_url2
                        WHERE {
                          <""" + subject1 + """> ?property ?object .
                          ?resource2 ?property ?object .
                          FILTER (""" + resource2_filter + """)
                          ?resource2 """ + varproperty + """ ?picture_url2  .
                          <""" + subject1 + """> """ + varproperty + """ ?picture_url  .
                        }
                        group by ?resource2 ?picture_url ?resource1 ?picture_url2
                        order by DESC(?relations)
                    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results

########################## def query2 ##########################
def query2(prefix, subject1, subject2, varproperty, sparqlendpoint):
    sparql = SPARQLWrapper(sparqlendpoint)
    sparql.setQuery("""
                        PREFIX """ + prefix + """
                        SELECT (<"""+ subject1 + """> AS ?resource1) (<""" + subject2 + """> AS ?resource2) (count(?object) as ?relations) ?picture_url ?picture_url2
                        WHERE {
                          <""" + subject1 + """> ?property ?object  .
                          <""" + subject2 + """> ?property ?object  .
                          <""" + subject1 + """> """ + varproperty + """ ?picture_url  .
                          <""" + subject2 + """> """ + varproperty + """ ?picture_url2  .
                        }
                        group by  ?resource1 ?resource2 ?picture_url ?picture_url2
                        ORDER BY DESC(?relations)
                    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results

########################## def query3 ##########################
def query3(prefix, resource1_filter, resource2_filter, varproperty, sparqlendpoint):
    sparql = SPARQLWrapper(sparqlendpoint)
    sparql.setQuery("""
                        PREFIX """ + prefix + """
                        SELECT ?resource1 (count(?object) as ?relations) ?resource2 ?picture_url ?picture_url2
                        WHERE {
                            ?resource1 ?property ?object  .
                            ?resource2 ?property ?object  .
                            ?resource1 """ + varproperty + """ ?picture_url  .
                            ?resource2 """ + varproperty + """ ?picture_url2  .
                            FILTER(""" + resource1_filter + """)
                            FILTER(""" + resource2_filter + """)
                            FILTER (str(?resource1) > str(?resource2))
                        }
                        group by  ?resource1 ?resource2 ?picture_url ?picture_url2
                        ORDER BY DESC(?relations)
                    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results

########################## call to query0 ##########################
# Detect the type of query (selected by user in index page)
if query_type == 'query0':
    start_time = time.time()
    # Default query to obtain at least an array of 10 elements, it executes 3 times
    # in order to show more than 10 relevant objects with certain similarity
    # Check if the base query (query0) is already in cache, and if not, stored it 
    if key0 in cache:
        results = cache.get(key0)
        print "resultado obtenido de cache q0"
    else:
        results = query0(prefix, uri, varproperty, sparql_endpoint)
        cache.set(key0, results)
        query_number += 1
        print "resultado almacenado en cache"
    # Make the query and save the result in "results" variable
    #results = query0(prefix, uri, varproperty, sparql_endpoint)
    # The following is a snippet because I haven't not test which option is better to delete
    # duplicated objects, I'm using set function, but I know I could use dictionary's properties as well
    #unique = { each['subject2']['value'] : each for each in results["results"]["bindings"] }.values()


    # Extract the results given by the response query using the json object given
    for result in results["results"]["bindings"]:
        # The objects that are related to the uri selected by the user or typed by
        qresources1.append(result["subject2"]['value'])
        # Number of relations betwee    n the uri and the result objects, it corresponds to
        # the number of diferent types of similar properties among them
        qrelations.append(result["relations"]['value'])
        # The object which property is an image, this image corresponds to subject2 object
        qpicture_url.append(result["picture_url"]['value'])
        # Delete duplicated object using set function
        #qresources1 = list(set(qresources1))
        #qpicture_url = list(set(qpicture_url))
        #qrelations = list(set(qrelations))
        # Select a random uri from the results of the previus queries
        uri = qresources1[randint(last_resource,len(qresources1)-1)]
        #Save into a csv file the results
        file.writerow([result["subject2"]['value']])
    #qresources1 = list(set(qresources1))
    #qpicture_url = list(set(qpicture_url))
    # Save the number of last object retrieved

    last_resource = (len(qresources1)-1)
    # Save the number of total objects retrieved
    qresources_total.append(len(qresources1))
    print qresources1 
    print qrelations
    end_time = time.time()
    elapsed_time = end_time - start_time

########################## call to query1 ##########################
elif query_type == 'query1':
    start_time = time.time()
    for i in range(0, iter_number):
        # 1st step: make a default query to retrieve at most 10 objects
        results = query0(prefix, uri, varproperty, sparql_endpoint)
        # Save the results in an array, I just save the result of "subject2" variable
        query_number += 1
       

        for result in results["results"]["bindings"]:
            qresources_default.append(result["subject2"]['value'])
            #qresources_default = list(set(qresources_default))
        # Create a filter to send it to the new query, this is a string with
        # the following form: ?resource2 = <http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/618/120621_104705F> || ?resource2 = <http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/1004/120621_104829F>"
        qresources_default = list(set(qresources_default))

        for j in range(1, len(qresources_default)-1):
            resource2_filter = resource2_filter + "?resource2 = <" + qresources_default[j] + "> || "
        resource2_filter = resource2_filter + "?resource2 = <" + qresources_default[len(qresources_default)-1] + ">"
        #print "long"
        #print len(qresources_default)
        # Make the queries until the length of the query results list, it means
        # that for each one of the elements we're going to make a query
        for k in range(0, len(qresources_default)-1):
            # Save in results the json object returned by query1, here we send to the function
            # the resource filter created before, now the uri will be the first element of the list qresources_default
            # and so on
            results = query1(prefix, qresources_default[k], resource2_filter, varproperty, sparql_endpoint)
            # Save in new arrays the results of the query, which includes the number of relations between the
            # selected uri and others resources, we retrieve the url of those mentioned before.
            query_number +=1

            for result in results["results"]["bindings"]:
                qresources1.append(result["resource1"]['value'])
                qrelations.append(result["relations"]['value'])
                qpicture_url.append(result["picture_url"]['value'])
                qresources2.append(result["resource2"]['value'])
                qpicture_url2.append(result["picture_url2"]['value'])
                duplicate_list.append(result["resource1"]['value']+","+result["relations"]['value']+","+result["picture_url"]['value']+","+result["resource2"]['value']+","+result["picture_url2"]['value'])

            # Update the filter to delete the first element of the list, which had been used in the query
            resource2_filter = resource2_filter.partition('||')[-1]
            #qresources_total.append(len(qresources1))
        delete_repeated = sorted(set(duplicate_list))
        uri = qresources_default[randint(last_resource,len(qresources_default)-1)]
        last_resource = (len(qresources_default)-1)
        #file.writerow(["----","----",0])
        end_time = time.time()
        elapsed_time = end_time - start_time
        iteration_number +=1
        print query_number
        print iteration_number
        statistics_file.writerow([query_type, elapsed_time, iter_number, iteration_number, len(qresources1),  len(duplicate_list)-len(delete_repeated), sum(map(int, qrelations)), query_number])
    
    for result in delete_repeated:
            a,b,c,d,e = result.split(',')
            file.writerow([a,b,c,d,e])
    print duplicate_list


########################## call to query2 ##########################
elif query_type == 'query2':
    start_time = time.time()
    for i in range(0,iter_number):
        results = query0(prefix, uri, varproperty, sparql_endpoint)
        query_number += 1

        for result in results["results"]["bindings"]:
            qresources_default.append(result["subject2"]['value'])

        qresources_default = list(set(qresources_default))

        for k in range(0, len(qresources_default)):
            for l in range(k+1, len(qresources_default)):
                results = query2(prefix, qresources_default[k], qresources_default[l], varproperty, sparql_endpoint)
                query_number += 1
                for result in results["results"]["bindings"]:
                    qresources2.append(result["resource2"]['value'])
                    qpicture_url2.append(result["picture_url2"]['value'])
                    qrelations.append(result["relations"]['value'])
                    qresources1.append(result["resource1"]['value'])
                    qpicture_url.append(result["picture_url"]['value'])

                    #qresources2 = list(set(qresources2))
                    #qpicture_url2 = list(set(qpicture_url2))

                #for result in results["results"]["bindings"]:
                    
                    #qresources1 = list(set(qresources1))
                    #qpicture_url1 = list(set(qpicture_url))
                    #uri = qresources1[randint(last_resource,len(qresources1)-1)]

                #duplicate_list.append(result["resource1"]['value']+result["resource2"]['value'])
                duplicate_list.append(result["resource1"]['value']+","+result["relations"]['value']+","+result["picture_url"]['value']+","+result["resource2"]['value']+","+result["picture_url2"]['value'])


            #Save into a csv file the results
            #for i in range(0, len(qresources1)):
            #    file.writerow([qresources1[i],qresources2[i],qrelations[i]])
            #qresources_total.append(len(qresources1))

        #Save into a csv file the results
        #for i in range(0, len(qresources1)):
         #   file.writerow([qresources1[i],qresources2[i],qrelations[i]])

        delete_repeated = sorted(set(duplicate_list))
        

        qresources_total.append(len(qresources1))
        uri = qresources_default[randint(last_resource,len(qresources_default)-1)]
        last_resource = (len(qresources_default)-1)
        end_time = time.time()
        elapsed_time = end_time - start_time
        iteration_number +=1
        print iteration_number
        print query_number
        statistics_file.writerow([query_type, elapsed_time, iter_number, iteration_number, len(qresources1),  len(duplicate_list)-len(delete_repeated), sum(map(int, qrelations)), query_number])
    for result in delete_repeated:
        a,b,c,d,e = result.split(',')
        file.writerow([a,b,c,d,e])

    print duplicate_list


########################## call to query3 ##########################
else:
    start_time = time.time()
    for i in range(0,iter_number):
        iteration_number += 1
        results = query0(prefix, uri, varproperty, sparql_endpoint)
        query_number += 1
        for result in results["results"]["bindings"]:
            qresources_default.append(result["subject2"]['value'])
        qresources_default = list(set(qresources_default))
        
        #if iteration_number <= 1:
        #    for j in range(0, len(qresources_default)-1):
        #        resource1_filter = resource1_filter + "?resource1 = <" + qresources_default[j] + "> || "
        #        resource2_filter = resource2_filter + "?resource2 = <" + qresources_default[j] + "> || "
        #    resource1_filter = resource1_filter + "?resource1 = <" + qresources_default[len(qresources_default)-1] + ">"
        #    resource2_filter = resource2_filter + "?resource2 = <" + qresources_default[len(qresources_default)-1] + ">"
        #else:
        #    for j in range(0, len(qresources_default)-1):
        #        resource1_filter = resource1_filter + " || ?resource1 = <" + qresources_default[j] + ">"
        #        resource2_filter = resource2_filter + "|| ?resource2 = <" + qresources_default[j] + ">"
        #    resource1_filter = resource1_filter + "|| ?resource1 = <" + qresources_default[len(qresources_default)-1] + ">"
        #    resource2_filter = resource2_filter + "|| ?resource2 = <" + qresources_default[len(qresources_default)-1] + ">"
        for j in range(0, len(qresources_default)-1):
            resource1_filter = resource1_filter + "?resource1 = <" + qresources_default[j] + "> || "
            resource2_filter = resource2_filter + "?resource2 = <" + qresources_default[j] + "> || "
        resource1_filter = resource1_filter + "?resource1 = <" + qresources_default[len(qresources_default)-1] + ">"
        resource2_filter = resource2_filter + "?resource2 = <" + qresources_default[len(qresources_default)-1] + ">"

        results = query3(prefix, resource1_filter, resource2_filter, varproperty, sparql_endpoint)
        query_number += 1
        for result in results["results"]["bindings"]:
            qresources1.append(result["resource1"]['value'])
            qpicture_url.append(result["picture_url"]['value'])
            qresources2.append(result["resource2"]['value'])
            qpicture_url2.append(result["picture_url2"]['value'])
            qrelations.append(result["relations"]['value'])
            print "pesos"
            print result["relations"]['value']

            
            #-------------> save into a csv file the results
            #file.writerow([result["resource1"]['value'],result["resource2"]['value'],result["relations"]['value']])
            #qresources1 = list(set(qresources1))
            duplicate_list.append(result["resource1"]['value']+result["resource2"]['value'])
            #duplicate_list.append(result["resource1"]['value']+","+result["relations"]['value']+","+result["picture_url"]['value']+","+result["resource2"]['value']+","+result["picture_url2"]['value'])
            #lista_temporal2.append(result["resource1"]['value']+","+result["relations"]['value']+","+result["picture_url"]['value']+","+result["resource2"]['value']+","+result["picture_url2"]['value'])
            lista_temporal2.append(result["resource1"]['value']+","+str(result["relations"]['value'])+","+result["picture_url"]['value']+","+result["resource2"]['value']+","+result["picture_url2"]['value'])
            
        
        #Save into a csv file the results
        #for i in range(0, len(qresources1)):
         #   file.writerow([qresources1[i],qresources2[i],qrelations[i]])
        qresources_total.append(len(qresources1))
        delete_repeated = sorted(set(duplicate_list))
        print "uri"
        print uri

        uri = qresources_default[randint(last_resource,len(qresources_default)-1)]
        
        resource1_filter = ""
        resource2_filter = ""
        last_resource = (len(qresources_default)-1)
        end_time = time.time()
        elapsed_time = end_time - start_time
        print iteration_number
        print query_number
        
        statistics_file.writerow([query_type, elapsed_time, iter_number, iteration_number, len(qresources1),  len(duplicate_list)-len(delete_repeated), sum(map(int, qrelations)), query_number])
    hilow = list(set(lista_temporal2))
    for result in hilow:
        a,b,c,d,e= result.split(',')
        file.writerow([a,b,c,d,e])
    
    

