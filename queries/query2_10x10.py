from SPARQLWrapper import SPARQLWrapper, JSON
import json, sets

concepts = []
qobjects = []
qrelations = []
qpicurl = []
qobjects_total = []
last_object = 0


sparql = SPARQLWrapper("http://semtech.mty.itesm.mx:3030/Fototeca/sparql")
prefix = "edm: <http://purl.org/dc/elements/1.1/>"
uri = "http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/1005/120621_104829F"
varproperty = "edm:identifier"
sparql.setQuery("""
                    PREFIX """ + prefix + """
                    SELECT ?picurl (count(?object) as ?relations) ?foto2 ?property1
                    WHERE { <""" + uri + """>
                        ?property1 ?object  .
                        OPTIONAL { ?foto2 """ + varproperty + """ ?picurl }.
                        <http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/1008/120621_104829F>
                            ?property2 ?object .
                        filter(?property1 = ?property2)
                        ?foto2 a <http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/Picture> .
                        filter(?property1 != <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>)
                        filter(<http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/1008/120621_104829F> != <""" + uri + """>)
                    }
                    group by ?foto2 ?picurl ?property1
                    ORDER BY DESC(?relations)
                    LIMIT 10
                """)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()




for result in results["results"]["bindings"]:
    qobjects.append(result["foto2"]['value'])
    qrelations.append(result["relations"]['value'])
    qpicurl.append(result["picurl"]['value'])
    qobjects = list(set(qobjects))

    print result["foto2"]['value'],result["picurl"]['value'],result["relations"]['value'],result["property1"]['value']
#print json.dumps(qobjects)
