from SPARQLWrapper import SPARQLWrapper, JSON
import json, sets

concepts = []
qobjects = []
qrelations = []
qpicurl = []
qobjects_total = []
last_object = 0


sparql = SPARQLWrapper("http://semtech.mty.itesm.mx:3030/Fototeca/sparql")
prefix = "edm: <http://purl.org/dc/elements/1.1/>"

uri2 = "http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/698/120621_104720F"
uri3 = "http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/823/120621_104753F"
varproperty = "edm:identifier"
sparql.setQuery("""
                    PREFIX """ + prefix + """
                    SELECT ?r2 (count(?object) as ?relations) ?picurl
                    WHERE {
                      <http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/698/120621_104720F> ?property ?object .
                      ?r2 ?property ?object .
                      filter (?r2 = <http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/698/120621_104720F> ||
                      ?r2 = <http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/823/120621_104753F>)
                      ?r2 """ + varproperty + """ ?picurl  .
                    }
                    group by ?r2 ?picurl
                    order by DESC(?relations)
                    LIMIT 10
                """)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()




for result in results["results"]["bindings"]:
    qobjects.append(result["r2"]['value'])
    qrelations.append(result["relations"]['value'])
    qpicurl.append(result["picurl"]['value'])
    qobjects = list(set(qobjects))

#    print result["r1"]['value'],result["r2"]['value'],result["relations"]['value'],result["picurl"]['value']
    print result["r2"]['value'],result["relations"]['value'],result["picurl"]['value']
#print json.dumps(qobjects)
