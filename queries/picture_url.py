from SPARQLWrapper import SPARQLWrapper, JSON
import json

picurl = []

sparql = SPARQLWrapper("http://semtech.mty.itesm.mx:3030/Fototeca/sparql")
uri = "http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/3807/120621_105651F"
sparql.setQuery("""
    PREFIX edm: <http://purl.org/dc/elements/1.1/>
    SELECT *
    WHERE {
     <""" + uri + """> edm:identifier ?o
    }
    group by ?o
""")
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

print results["results"]["bindings"]
