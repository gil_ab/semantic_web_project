from SPARQLWrapper import SPARQLWrapper, JSON
import json, sets

concepts = []
qobjects = []
qobjects2 = []
qrelations = []
qpicture_url = []
qpicture_url2 = []
qobjects_total = []
last_object = 0


sparql = SPARQLWrapper("http://semtech.mty.itesm.mx:3030/Fototeca/sparql")
prefix = "edm: <http://purl.org/dc/elements/1.1/>"

subject1 = "http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/698/120621_104720F"
subject2 = "http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/823/120621_104753F"
varproperty = "edm:identifier"
sparql.setQuery("""
                    PREFIX """ + prefix + """
                    SELECT (<"""+ subject1 + """> AS ?resource1) (<""" + subject2 + """> AS ?resource2) (count(?object) as ?relations) ?picture_url ?picture_url2
                    WHERE {
                      <""" + subject1 + """> ?property ?object  .
                      <""" + subject2 + """> ?property ?object  .
                      <""" + subject1 + """> """ + varproperty + """ ?picture_url  .
                      <""" + subject2 + """> """ + varproperty + """ ?picture_url2  .
                    }
                    group by  ?resource1 ?resource2 ?picture_url ?picture_url2
                    ORDER BY DESC(?relations)
                    LIMIT 10
                """)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()




for result in results["results"]["bindings"]:
    qobjects.append(result["resource1"]['value'])
    qrelations.append(result["relations"]['value'])
    qobjects2.append(result["resource2"]['value'])
    qpicture_url.append(result["picture_url"]['value'])
    qpicture_url2.append(result["picture_url2"]['value'])
    qobjects = list(set(qobjects))

    print result["resource1"]['value'], result["picture_url"]['value'],result["resource2"]['value'], result["picture_url2"]['value'],result["relations"]['value'],
#print json.dumps(qobjects)
