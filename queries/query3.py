from SPARQLWrapper import SPARQLWrapper, JSON
import json, sets

concepts = []
qobjects = []
qrelations = []
qpicurl = []
qobjects_total = []
last_object = 0


sparql = SPARQLWrapper("http://semtech.mty.itesm.mx:3030/Fototeca/sparql")
prefix = "edm: <http://purl.org/dc/elements/1.1/>"
uri = "http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/1005/120621_104829F"
uri2 = "http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/698/120621_104720F"
uri3 = "http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/823/120621_104753F"
varproperty = "edm:identifier"
sparql.setQuery("""
                    PREFIX """ + prefix + """
                    SELECT ?r1 (count(?object) as ?relations) ?r2 ?picurl
                    WHERE {
                        ?r1 ?property1 ?object  .
                        ?r2 ?property1 ?object  .
                        OPTIONAL { ?r1 """ + varproperty + """ ?picurl }.
                        filter(?r1 = <""" + uri2 + """> || ?r1 = <""" + uri3 + """>)
                        filter(?r2 = <""" + uri2 + """> || ?r2 = <""" + uri3 + """>)
                    }
                    group by  ?r1 ?r2 ?picurl
                    ORDER BY DESC(?relations)
                    LIMIT 10
                """)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()




for result in results["results"]["bindings"]:
    qobjects.append(result["r1"]['value'])
    qrelations.append(result["relations"]['value'])
    qpicurl.append(result["r2"]['value'])
    qobjects = list(set(qobjects))

    print result["r1"]['value'],result["r2"]['value'],result["relations"]['value'], result["picurl"]['value']
#print json.dumps(qobjects)
