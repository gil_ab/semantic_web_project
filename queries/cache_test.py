from django.core.cache import cache
from django.views.decorators.cache import cache_page
from memcached_stats import MemcachedStats
from django.conf import settings
from django.core.cache.backends import locmem


settings.configure()

cache.set('hola', "tarola")
print cache.get('hola')

# print out all your keys
mem = MemcachedStats() 
print mem.keys()

if "hola" in cache:
	print "si esta en cache"
else:
	print "no esta en cache"
print locmem._caches
print cache.keys()