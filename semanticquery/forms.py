from django import forms

QUERY_OPTIONS = (
    ('query0', 'Basic search'),
    ('query1', 'Query type 1'),
    ('query2', 'Query type 2'),
    ('query3', 'Query type 3'),
)

CHOICES = (('1', '1'),('2', '2'),('3', '3'),('4', '4'),('5', '5'),('6', '6'),('7', '7'),('8', '8'),('9', '9'),('10', '10'),('20', '20'),('30', '30'), ('50','50'),('100', '100'),)

class SearchForm(forms.Form):
    sparql_endpoint = forms.CharField(max_length=500, widget=forms.TextInput(attrs={'class': 'form-control'}), initial="http://semtech.mty.itesm.mx:3030/Fototeca/sparql")
    uri = forms.CharField(max_length=500, widget=forms.TextInput(attrs={'class': 'form-control'}), initial="http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/1005/120621_104829F" )
    prefix = forms.CharField(max_length=500, widget=forms.TextInput(attrs={'class': 'form-control'}), initial="edm: <http://purl.org/dc/elements/1.1/>" )
    varProperty = forms.CharField(max_length=500, widget=forms.TextInput(attrs={'class': 'form-control'}), initial="edm:identifier")
    #query_type = forms.ChoiceField(required=True, widget=forms.RadioSelect, choices=QUERY_OPTIONS, initial=1)
    query_type = forms.ChoiceField(required = True, choices = QUERY_OPTIONS, widget=forms.RadioSelect(attrs={'class' : 'Radio'}), initial=1)
    iter_number = forms.ChoiceField(choices=CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))
