from django.shortcuts import render
from django.http import HttpResponse
from semanticquery.forms import SearchForm
from django.http import HttpResponseRedirect
from SPARQLWrapper import SPARQLWrapper, JSON
from django.utils.safestring import mark_safe
import json, sets, re
from random import randint
import csv, time, hashlib
from django.core.cache import cache
from memcached_stats import MemcachedStats

def index(request):
    # Return a rendered response to send to the client.
    form = SearchForm(request.GET or None)
    context = {
        "form": form
    }
    # Print form.cleaned_data
    if form.is_valid():
        # Save values from form
        sparql_endpoint = form.data['sparql_endpoint']
        uri = form.data['uri']
        prefix = form.data['prefix']
        varproperty = form.data['varProperty']
        query_type = form.data['query_type']
        iter_number = int(form.data['iter_number'])
        # Global variables
        qresources1 = [] # Resources retrieved for certain queries 
        qresources2 = [] # Resources retrieved for certain queries 
        qrelations = [] # Count of relations as result of a query
        qpicture_url = [] # Picture URLS retrevied for certain queries for resource 1
        qpicture_url2 = [] # Picture URLS retrevied for certain queries for resource 2
        qresources_total = [] # Counter of total resources retrieved from a specific query: 1, 2 or 3
        qresources_default = [] # Counter of total resources retrieved from query0
        resource1_filter = "" # Filter for certain queries
        resource2_filter = "" # Filter for certain queries
        repeated_edges = [] # Count all the edges retrieved, including repeated ones
        iteration_number = 0 # Counter of the number of iterations 
        query_number = 0 # Counter of the total of queries done
        query_saved = 0 # Counter of the queries not done
        key0 = uri # Key for store query0 results in memcached
        key1 = hashlib.md5() # Key for store in memcached the results of the other types of queries: 1,2 or 3
        weights_sum = [] # Store the weights of the edges retrieved in each iteration to save the results in the 
                            # statistics file
        executionTime = 50
        executionTime2 = 1
        ########################## call to query0 ##########################
        # Detect the type of query (selected by user in index page)
        if query_type == 'query0':
            start_time = time.time()
            # Default query to obtain at least an array of 10 elements
            # Check if the base query (query0) is already in cache, and if not
            #  make the query and save the result in "results" variable and store them in the cache
            if key0 in cache:
                results = cache.get(key0)
                query_saved += 1
            else:
                results = query0(prefix, uri, varproperty, sparql_endpoint)
                cache.set(key0, results)
                query_number += 1
            
            # Extract the results given by the response query using the json object given
            for result in results["results"]["bindings"]:
                # The objects that are related to the uri selected by the user or typed by
                qresources1.append(result["subject2"]['value'])
                # Number of relations between the uri and the result objects, it corresponds to
                # the number of diferent types of similar properties among them
                qrelations.append(result["relations"]['value'])
                # The object which property is an image, this image corresponds to subject2 object
                qpicture_url.append(result["picture_url"]['value'])
                #Save into a csv file the results (but not required)
                #file.writerow([result["subject2"]['value']])

            # Save the number of total objects retrieved
            qresources_total.append(len(qresources1))
            # Calculate elapsed time
            end_time = time.time()
            elapsed_time = end_time - start_time

            # Creates a context variable with the variables and values to send as a function's response
            context_dict = { 'query_objects': json.dumps(qresources1), 'query_picture_url': json.dumps(qpicture_url), 'query_objects2': json.dumps(qresources2), 'query_picture_url2': json.dumps(qpicture_url2), 'query_relations': json.dumps(qrelations), 'uri_view': uri, 'resultados': json.dumps(results["results"]["bindings"]), 'query_type': query_type, 'elapsed_time': round(elapsed_time,2), 'iter_number': iter_number, 'total_edges':len(qresources1), 'repeated_edges': "NA", 'unique_edges': "NA", 'weight_sumation': sum(map(int, qrelations)), 'query_number': query_number }            
            # It sends the arrays with the queries' results to be rendered in the html page
            return render(request, 'semanticquery/search_query_results.html', context_dict)

        ########################## call to query1 ##########################
        elif query_type == 'query1':
            statistics_file = csv.writer(open("./queries_results_cache/" + query_type + "_ex_" + str(executionTime) + "_" + str(iter_number) + "xx_statistics.csv", "wb+"))
            #statistics_file = csv.writer(open("./queries_results/" + query_type + "_ex_" + str(executionTime) + "_" + str(iter_number) + "x_statistics.csv", "wb+"))
            for n in range(1, executionTime+1):
                iter_number = int(form.data['iter_number'])
                # Global variables
                #uri = form.data['uri']
                qresources1 = [] # Resources retrieved for certain queries 
                qresources2 = [] # Resources retrieved for certain queries 
                qrelations = [] # Count of relations as result of a query
                qpicture_url = [] # Picture URLS retrevied for certain queries for resource 1
                qpicture_url2 = [] # Picture URLS retrevied for certain queries for resource 2
                qresources_total = [] # Counter of total resources retrieved from a specific query: 1, 2 or 3
                qresources_default = [] # Counter of total resources retrieved from query0
                resource1_filter = "" # Filter for certain queries
                resource2_filter = "" # Filter for certain queries
                repeated_edges = [] # Count all the edges retrieved, including repeated ones
                iteration_number = 0 # Counter of the number of iterations 
                query_number = 0 # Counter of the total of queries done
                query_saved = 0 # Counter of the queries not done
                key0 = uri # Key for store query0 results in memcached
                key1 = hashlib.md5() # Key for store in memcached the results of the other types of queries: 1,2 or 3
                weights_sum = [] # Store the weights of the 
                unique_edges = []
                a = []
                 

                start_time = time.time()
                # Create the files to store the results
                #statistics_file = csv.writer(open("./queries_results/" + query_type + "_" + str(iter_number) + "x_statistics.csv", "wb+"))
                #results_file = csv.writer(open("./queries_results/" + query_type + "_" +  str(iter_number) + "x_results.csv", "wb+"))
                
                #results_file = csv.writer(open("./queries_results_cache/" + query_type + "_ex_"  + str(executionTime2) + "_" + str(iter_number) + "x_results.csv", "wb+"))
                for i in range(0, iter_number):
                    # 1st step: make a default query to retrieve at most 10 objects, checks in cache as
                    # described before
                    if key0 in cache:
                        results = cache.get(key0)
                        query_saved += 1
                    else:
                        results = query0(prefix, uri, varproperty, sparql_endpoint)
                        cache.set(key0, results)
                        query_number += 1

                   
                    for result in results["results"]["bindings"]:
                        qresources_default.append(result["subject2"]['value'])                
                    qresources_default = list(set(qresources_default))

                    # Create a filter to send it to the new query, this is a string with
                    # the following form: ?resource2 = <http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/618/120621_104705F> || ?resource2 = <http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/1004/120621_104829F>"
                    for j in range(1, len(qresources_default)-1):
                        resource2_filter = resource2_filter + "?resource2=<" + qresources_default[j] + ">||"
                    resource2_filter = resource2_filter + "?resource2=<" + qresources_default[len(qresources_default)-1] + ">"
                    
                    # Make the queries until the length of the query results list, it means
                    # that for each one of the elements we're going to make a query
                    for k in range(0, len(qresources_default)-1):
                        key1.update(resource2_filter)
                        if key1.hexdigest() in cache:
                            results = cache.get(key1.hexdigest())
                            query_saved += 1
                        else:
                            results = query1(prefix, qresources_default[k], resource2_filter, varproperty, sparql_endpoint)
                            cache.set(key1.hexdigest(), results)
                            query_number +=1
                        
                        # Save in results the json object returned by query1, here we send to the function
                        # the resource filter created before, now the uri will be the first element of the list qresources_default
                        # and so on
                        # Save in a list (duplicated_list) the results of the query, which includes the number of relations between the
                        # selected uri and others resources, we retrieve the url of those mentioned before.
                        for result in results["results"]["bindings"]:
                            repeated_edges.append(result["relations"]['value']+","+result["resource1"]['value']+","+result["picture_url"]['value']+","+result["resource2"]['value']+","+result["picture_url2"]['value'])

                        # Update the filter to delete the first element of the list, which had been used in the query
                        resource2_filter = resource2_filter.partition('||')[-1]

                    # Create a new list withouth the repeated edges    
                    unique_edges = sorted(set(repeated_edges))
                    # Update the new pivot 
                    uri = qresources_default[randint(0,len(qresources_default)-1)]
                    # Update the key with the new pivot
                    key0 = uri
                    end_time = time.time()
                    elapsed_time = end_time - start_time
                    iteration_number +=1
                    # Print in console the iteration number
                    print "Iteration number: " + str(iteration_number)

                    # Save the results of counted relations to sum weights
                    for result in unique_edges:
                        a = result.split(',')[0]
                        weights_sum.append(a)   
                    # Create an statistics file 
                    
                    #statistics_file.writerow([query_type, elapsed_time, iter_number, iteration_number, len(repeated_edges),  len(repeated_edges)-len(unique_edges), len(unique_edges), sum(map(int, weights_sum)), query_number, query_saved])
                statistics_file.writerow([query_type, elapsed_time, iter_number, iteration_number, len(repeated_edges),  len(repeated_edges)-len(unique_edges), len(unique_edges), sum(map(int, weights_sum)), query_number, query_saved])
                executionTime2 += 1

                    
                    



            # Finally create a list witht all the results to render in html results page
            

            context_dict = { 'query_objects': json.dumps(qresources1), 'query_picture_url': json.dumps(qpicture_url), 
            'query_objects2': json.dumps(qresources2), 'query_picture_url2': json.dumps(qpicture_url2), 
            'query_relations': json.dumps(qrelations), 'uri_view': uri, 'resultados': json.dumps(results["results"]["bindings"]),
            'query_type': query_type, 'elapsed_time': round(elapsed_time,2), 'iter_number': iter_number, 'total_edges':len(repeated_edges), 'repeated_edges': len(repeated_edges)-len(unique_edges), 'unique_edges': len(repeated_edges) - (len(repeated_edges)-len(unique_edges)), 'weight_sumation': sum(map(int, qrelations)), 'query_number': query_number }
            
            return render(request, 'semanticquery/search_query_results.html', context_dict)
            

        ########################## call to query2 ##########################
        elif query_type == 'query2':
            statistics_file = csv.writer(open("./queries_results_cache/" + query_type + "_ex_" + str(executionTime) + "_" + str(iter_number) + "xx_statistics.csv", "wb+"))
            #statistics_file = csv.writer(open("./queries_results/" + query_type + "_ex_" + str(executionTime) + "_" + str(iter_number) + "x_statistics.csv", "wb+"))
            for n in range(1, executionTime+1):
                iter_number = int(form.data['iter_number'])
                # Global variables
                #uri = form.data['uri']
                qresources1 = [] # Resources retrieved for certain queries 
                qresources2 = [] # Resources retrieved for certain queries 
                qrelations = [] # Count of relations as result of a query
                qpicture_url = [] # Picture URLS retrevied for certain queries for resource 1
                qpicture_url2 = [] # Picture URLS retrevied for certain queries for resource 2
                qresources_total = [] # Counter of total resources retrieved from a specific query: 1, 2 or 3
                qresources_default = [] # Counter of total resources retrieved from query0
                resource1_filter = "" # Filter for certain queries
                resource2_filter = "" # Filter for certain queries
                repeated_edges = [] # Count all the edges retrieved, including repeated ones
                iteration_number = 0 # Counter of the number of iterations 
                query_number = 0 # Counter of the total of queries done
                query_saved = 0 # Counter of the queries not done
                key0 = uri # Key for store query0 results in memcached
                key1 = hashlib.md5() # Key for store in memcached the results of the other types of queries: 1,2 or 3
                weights_sum = [] # Store the weights of the 
                unique_edges = []
                a = []

                #statistics_file = csv.writer(open("./queries_results/" + query_type + "_" + str(iter_number) + "x_statistics.csv", "wb+"))
                #results_file = csv.writer(open("./queries_results/" + query_type + "_" +  str(iter_number) + "x_results.csv", "wb+"))
                start_time = time.time()
                for i in range(0,iter_number):
                    if key0 in cache:
                        results = cache.get(key0)
                        query_saved += 1
                    else:
                        results = query0(prefix, uri, varproperty, sparql_endpoint)
                        cache.set(key0, results)
                        query_number += 1

                    for result in results["results"]["bindings"]:
                        qresources_default.append(result["subject2"]['value'])
                    qresources_default = list(set(qresources_default))

                    for k in range(0, len(qresources_default)):
                        for l in range(k+1, len(qresources_default)):
                            key1.update(qresources_default[k]+qresources_default[l])
                            if key1.hexdigest() in cache:
                                results = cache.get(key1.hexdigest())
                                query_saved += 1
                            else:
                                results = query2(prefix, qresources_default[k], qresources_default[l], varproperty, sparql_endpoint)
                                cache.set(key1.hexdigest(), results)
                                query_number +=1

                            for result in results["results"]["bindings"]:
                                repeated_edges.append(result["relations"]['value']+","+result["resource1"]['value']+","+result["picture_url"]['value']+","+result["resource2"]['value']+","+result["picture_url2"]['value'])

                    qresources_total.append(len(repeated_edges))
                    unique_edges = sorted(set(repeated_edges))
                    uri = qresources_default[randint(0,len(qresources_default)-1)]
                    key0 = uri
                    end_time = time.time()
                    elapsed_time = end_time - start_time
                    iteration_number +=1
                    # Print in console the iteration number
                    print "Iteration number: " + str(iteration_number)
                    for result in unique_edges:
                        a = result.split(',')[0]
                        weights_sum.append(a)

                statistics_file.writerow([query_type, elapsed_time, iter_number, iteration_number, len(repeated_edges),  len(repeated_edges)-len(unique_edges), len(unique_edges), sum(map(int, weights_sum)), query_number, query_saved])
                weights_sum = []
                executionTime2 += 1
                

            context_dict = { 'query_objects': json.dumps(qresources1), 'query_picture_url': json.dumps(qpicture_url), 
            'query_objects2': json.dumps(qresources2), 'query_picture_url2': json.dumps(qpicture_url2), 
            'query_relations': json.dumps(qrelations), 'uri_view': uri, 'resultados': json.dumps(results["results"]["bindings"]),
            'query_type': query_type, 'elapsed_time': round(elapsed_time,2), 'iter_number': iter_number, 'total_edges':len(repeated_edges), 'repeated_edges': len(repeated_edges)-len(unique_edges), 'unique_edges': len(repeated_edges) - (len(repeated_edges)-len(unique_edges)), 'weight_sumation': sum(map(int, qrelations)), 'query_number': query_number }
            return render(request, 'semanticquery/search_query_results.html', context_dict)

        ########################## call to query3 ##########################
        else:
            #statistics_file = csv.writer(open("./queries_results/" + query_type + "_" + str(iter_number) + "x_statistics.csv", "wb+"))
            #results_file = csv.writer(open("./queries_results/" + query_type + "_" +  str(iter_number) + "x_results.csv", "wb+"))
            #statistics_file = csv.writer(open("./queries_results/" + query_type + "_ex_" + str(executionTime) + "_" + str(iter_number) + "x_statistics.csv", "wb+"))
            statistics_file = csv.writer(open("./queries_results_cache/" + query_type + "_ex_" + str(executionTime) + "_" + str(iter_number) + "xx_statistics.csv", "wb+"))
            for n in range(1, executionTime+1):
                iter_number = int(form.data['iter_number'])
                # Global variables
                #uri = form.data['uri']
                qresources1 = [] # Resources retrieved for certain queries 
                qresources2 = [] # Resources retrieved for certain queries 
                qrelations = [] # Count of relations as result of a query
                qpicture_url = [] # Picture URLS retrevied for certain queries for resource 1
                qpicture_url2 = [] # Picture URLS retrevied for certain queries for resource 2
                qresources_total = [] # Counter of total resources retrieved from a specific query: 1, 2 or 3
                qresources_default = [] # Counter of total resources retrieved from query0
                resource1_filter = "" # Filter for certain queries
                resource2_filter = "" # Filter for certain queries
                repeated_edges = [] # Count all the edges retrieved, including repeated ones
                iteration_number = 0 # Counter of the number of iterations 
                query_number = 0 # Counter of the total of queries done
                query_saved = 0 # Counter of the queries not done
                key0 = uri # Key for store query0 results in memcached
                key1 = hashlib.md5() # Key for store in memcached the results of the other types of queries: 1,2 or 3
                weights_sum = [] # Store the weights of the 
                unique_edges = []
                a = []

                start_time = time.time()
                for i in range(0,iter_number):
                    iteration_number += 1

                    if key0 in cache:
                        results = cache.get(key0)
                        query_saved += 1
                    else:
                        results = query0(prefix, uri, varproperty, sparql_endpoint)
                        cache.set(key0, results)
                        query_number += 1
                    
                    for result in results["results"]["bindings"]:
                        qresources_default.append(result["subject2"]['value'])
                    qresources_default = list(set(qresources_default))
                    for j in range(0, len(qresources_default)-1):
                        resource1_filter = resource1_filter + "?resource1 = <" + qresources_default[j] + "> || "
                        resource2_filter = resource2_filter + "?resource2 = <" + qresources_default[j] + "> || "
                    resource1_filter = resource1_filter + "?resource1 = <" + qresources_default[len(qresources_default)-1] + ">"
                    resource2_filter = resource2_filter + "?resource2 = <" + qresources_default[len(qresources_default)-1] + ">"
                    key1.update(resource1_filter+resource2_filter)
                    if key1.hexdigest() in cache:
                        results = cache.get(key1.hexdigest())
                        query_saved += 1
                    else:
                        results = query3(prefix, resource1_filter, resource2_filter, varproperty, sparql_endpoint)
                        cache.set(key1.hexdigest(), results)
                        query_number +=1

                    for result in results["results"]["bindings"]:
                        repeated_edges.append(result["relations"]['value']+","+result["resource1"]['value']+","+result["picture_url"]['value']+","+result["resource2"]['value']+","+result["picture_url2"]['value'])
                    
                    qresources_total.append(len(qresources1))
                    unique_edges = sorted(set(repeated_edges))
                    uri = qresources_default[randint(0,len(qresources_default)-1)]
                    key0 = uri
                    resource1_filter = ""
                    resource2_filter = ""
                    end_time = time.time()
                    elapsed_time = end_time - start_time
                    print iteration_number
                    print query_number
                    
                    for result in unique_edges:
                        a = result.split(',')[0]
                        weights_sum.append(a)
                statistics_file.writerow([query_type, elapsed_time, iter_number, iteration_number, len(repeated_edges),  len(repeated_edges)-len(unique_edges), len(unique_edges), sum(map(int, weights_sum)), query_number, query_saved])
                weights_sum = []
                

            context_dict = { 'query_objects': json.dumps(qresources1), 'query_picture_url': json.dumps(qpicture_url), 
            'query_objects2': json.dumps(qresources2), 'query_picture_url2': json.dumps(qpicture_url2), 
            'query_relations': json.dumps(qrelations), 'uri_view': uri, 'resultados': json.dumps(results["results"]["bindings"]),
            'query_type': query_type, 'elapsed_time': round(elapsed_time,2), 'iter_number': iter_number, 'total_edges':len(repeated_edges), 'repeated_edges': len(repeated_edges)-len(unique_edges), 'unique_edges': len(repeated_edges) - (len(repeated_edges)-len(unique_edges)), 'weight_sumation': sum(map(int, qrelations)), 'query_number': query_number }
            return render(request, 'semanticquery/search_query_results.html', context_dict)

    return render(request, 'semanticquery/index.html', context)

def search_query(request):
    form = SearchForm(request.GET or None)
    context = {
        "form": form
    }
    if form.is_valid():
        # print form.cleaned_data
        sparql_endpoint = form.data['sparql_endpoint']
        uri = form.data['uri']
        context_dict = {'sparql_endpoint': sparql_endpoint}
        #return HttpResponseRedirect('/semanticquery/search_query_results/')
        return render(request, 'semanticquery/search_query_results.html', context_dict)
    return render(request, 'semanticquery/search_query.html', context)

########################## def query0 ##########################
def query0(prefix, uri, varproperty, sparqlendpoint):
    sparql = SPARQLWrapper(sparqlendpoint)
    sparql.setQuery("""
                        PREFIX """ + prefix + """
                        SELECT ?picture_url (count(?object) as ?relations) ?subject2
                        WHERE { <""" + uri + """>
                            ?property1 ?object  .
                            OPTIONAL { ?subject2 """ + varproperty + """ ?picture_url }.
                            ?subject2 ?property2 ?object .
                            FILTER(?property1 = ?property2)
                            ?subject2 a <http://semtech.mty.itesm.mx:8888/marmotta/resource/fototeca/Picture> .
                            FILTER(?property1 != <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>)
                            FILTER(?subject2 != <""" + uri + """>)
                        }
                        group by ?subject2 ?picture_url
                        ORDER BY DESC(?relations)
                        LIMIT 10
                    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results

########################## def query1 ##########################
def query1(prefix, subject1, resource2_filter, varproperty, sparqlendpoint):
    sparql = SPARQLWrapper(sparqlendpoint)
    sparql.setQuery("""
                        PREFIX """ + prefix + """
                        SELECT ?resource2 (count(?object) as ?relations) (<"""+ subject1 + """> AS ?resource1) ?picture_url ?picture_url2
                        WHERE {
                          <""" + subject1 + """> ?property ?object .
                          ?resource2 ?property ?object .
                          FILTER (""" + resource2_filter + """)
                          ?resource2 """ + varproperty + """ ?picture_url2  .
                          <""" + subject1 + """> """ + varproperty + """ ?picture_url  .
                        }
                        group by ?resource2 ?picture_url ?resource1 ?picture_url2
                        order by DESC(?relations)
                    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results

########################## def query2 ##########################
def query2(prefix, subject1, subject2, varproperty, sparqlendpoint):
    sparql = SPARQLWrapper(sparqlendpoint)
    sparql.setQuery("""
                        PREFIX """ + prefix + """
                        SELECT (<"""+ subject1 + """> AS ?resource1) (<""" + subject2 + """> AS ?resource2) (count(?object) as ?relations) ?picture_url ?picture_url2
                        WHERE {
                          <""" + subject1 + """> ?property ?object  .
                          <""" + subject2 + """> ?property ?object  .
                          <""" + subject1 + """> """ + varproperty + """ ?picture_url  .
                          <""" + subject2 + """> """ + varproperty + """ ?picture_url2  .
                        }
                        group by  ?resource1 ?resource2 ?picture_url ?picture_url2
                        ORDER BY DESC(?relations)
                    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results

########################## def query3 ##########################
def query3(prefix, resource1_filter, resource2_filter, varproperty, sparqlendpoint):
    sparql = SPARQLWrapper(sparqlendpoint)
    sparql.setQuery("""
                        PREFIX """ + prefix + """
                        SELECT ?resource1 (count(?object) as ?relations) ?resource2 ?picture_url ?picture_url2
                        WHERE {
                            ?resource1 ?property ?object  .
                            ?resource2 ?property ?object  .
                            ?resource1 """ + varproperty + """ ?picture_url  .
                            ?resource2 """ + varproperty + """ ?picture_url2  .
                            FILTER(""" + resource1_filter + """)
                            FILTER(""" + resource2_filter + """)
                            FILTER (str(?resource1) > str(?resource2))
                        }
                        group by  ?resource1 ?resource2 ?picture_url ?picture_url2
                        ORDER BY DESC(?relations)
                    """)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results

def about(request):
    return HttpResponse("Rango says here is the about page <a href='/semanticquery'>Home</a> ")
    context_dict = {'name': "Gil"}
    return render(request, 'semanticquery/about.html', context=context_dict)