# Semantic web application 

This repository is part of the master thesis project developed by Gilberto Ayala during his research at Tecnológico de Monterrey, campus Monterrey. This web app is left intact in order to replicate the results reported in the thesis file. A new branch called "semantic_web_app" was created in order to continue the development and new experiments mainly with the students of summer reasearch, but of course, it is open to the community to improve and suggest new functionalities.
